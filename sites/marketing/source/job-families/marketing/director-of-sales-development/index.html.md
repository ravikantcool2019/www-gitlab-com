---
layout: job_family_page
title: "Director, Global Sales Development"
---

## Levels

### Director, Global Sales Development

The Director, Global Sales Development reports to the [Vice President of Revenue Marketing](/job-families/marketing/vp-of-revenue-marketing/).

#### Director, Global Sales Development Job Grade 

The Director, Sales Development is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, Global Sales Development Responsibilities

- Ensure sales accepted opportunities are sourced in accordance with company targets, and that our sales organization has enough opportunities to work with to be fully productive.
- Ensure we efficiently and effectively qualify inbound demand as well develop outbound strategies per region while leveraging digital and field marketing resources for particular campaigns
- Motivate and develop SDRs in alignment to our leveling concept in order to exceed goals through coaching and incentives and grow their careers. A successful candidate will be equal parts business manager and talent developer. 
- Plan, forecast, and understand ramp adjusted capacity to ensure the team is grown effectively, in tandem with the needs of our sales organization and the supply of inbound demand from marketing.
- Manage SDR rosters for tracking ramp adjusted capacity and productivity.
- Manage SDR reports and dashboards to ensure the results they deliver can be easily understood by stakeholders throughout the organization.
- Build a word-class sales and business development team. Recruit, train, and develop a global team of SDRs, and SDR managers.
- Effectively manage paths for career advancement within the SDR functions as well as to closing sales roles.
- Partner with regional managers to develop campaigns that align to their regional goals based on a territory plan.
- Iterating the sales development process, methodology, campaigns, hiring profiles, training and enablement with respect to the GitLab core values.
- Work closely with marketing, sales, and people ops to ensure SDR onboarding program and ongoing training is up-to-date on our current product offering.
- Partner with sales and marketing operations to ensure the SDR team has the best tools to do their job, and that they are configured to ensure SDR efficiency and productivity, especially salesforce.com and Outreach.
- Focus on the quality of opportunities without sacrificing quantity.  The four core metrics this role should care about are the impact on the sales cycle, close rates, pipeline contribution and average deal size.




#### Director, Global Sales Development Requirements

- Proven track record of delivering sales pipeline at large enterprise accounts through leading outbound prospecting teams.
- Responsible for creating and iterating the sales development process, methodology, campaigns, hiring profiles, training and enablement
- Experience managing an outbound prospecting team at world wide with at least 30 total direct and indirect reports.
- Power user of salesforce.com and SDR cadence management software.
- Ability to drive cross functional alignment and coordination across sales and marketing teams.
- Ability to attract, retain, and motivate exceptional SDRs and SDR managers.
- Have a general understanding of Git, GitLab, and modern development practices.
- A broad knowledge of the application development ecosystem.
- Awareness of industry trends in enterprise digital transformation, devops, and continuous integration.
- Excellent written and spoken English.
- Accurate, nuanced, direct, and kind messaging.
- Being able to work independent and respond quickly.
- Able to articulate the GitLab mission, values, and vision.
- [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#director-group)
- Ability to use GitLab

Also, we know it’s tough, but please try to avoid the confidence gap. You don’t have to match all the listed requirements exactly to apply! We value diverse experience, so go for it and submit an application if GitLab sounds like a place you’d like to join.

## Performance Indicators

- Improvement in pipeline values
- Sales Accepted Opportunity attainment vs. goal
- Improvement in Marketing Qualified Lead conversion rates
- Improvement in sales close rates for SDR generated Opportunities
- Improvement in SDR Efficiency and return on investment

## Career Ladder

The next step in the Director, Sales Development job family is not yet defined at GitLab. 

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Candidates will then meet with the Senior Director of Revenue Marketing.
* Candidates will then be asked to schedule an interview with the Manager of Field Marketing, Americas.
* Candidates will then meet with the CMO.
* Following successful completion of the previous steps, final candidates will then be asked to meet with a panel of Sales Development Managers.
* Final candidates may be asked to complete an assessment, and  meet with the Senior Director of Revenue Marketing again.
* Successful candidates will subsequently be made an offer via video or phone.

Additional details about our process can be found on our [hiring page](/handbook/hiring).

### Relevant links

- [Sales Development Handbook](/handbook/marketing/revenue-marketing/sdr/)
