---
layout: handbook-page-toc
title: "Sales Plays"
description: “Discover GitLab Solutions Architects’ Sales Plays practices”
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**SA Practices**](/handbook/customer-success/solutions-architects/sa-practices) - [**Sales Plays**](/handbook/customer-success/solutions-architects/sales-plays) - [**Tools and Resources**](/handbook/customer-success/solutions-architects/tools-and-resources) - [**Career Development**](/handbook/customer-success/solutions-architects/career-development) - [**Demonstration**](/handbook/customer-success/solutions-architects/demonstrations) - [**Processes**](/handbook/customer-success/solutions-architects/processes) - [**Education and Enablement**](/handbook/customer-success/education-enablement)

## Sales Plays

The SA should either be included in a discovery call or provided with Outcome / Infrastructure / Challenges  information uncovered by prior interactions with the account.  Occasionally, SA's may support a combination call of discovery and technical demonstration/deep-dive on a single call, but this is suboptimal as the latter approach does not allow the SA time to prepare for and/or tailor the discussion.

The SA is also responsible for any pre-sales technical customer inquiry or audit from associated accounts, including RFI, RFP or security audits. For details on the audit process, proceed to the [Security](/handbook/engineering/security/security-assurance/risk-field-security/customer-security-assessment-process.html) page.

The SA is always responsible for drafting Professional Services SOW's, regardless of the account status (pre- and post-sales).

As a general guideline, no more than 2 Solutions Architects should join any single client-facing call, whether it be assistive for the topic or for learning / shadowing.

### SALES PLAYS [TODO]

1. QUALITY
2. DISCOVERY
3. TECHNICAL EVALUATION
4. PROPOSAL
5. NEGOTIATE
6. CLOSE
