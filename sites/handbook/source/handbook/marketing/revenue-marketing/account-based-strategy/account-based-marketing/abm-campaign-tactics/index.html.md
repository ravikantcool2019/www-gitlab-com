---
layout: handbook-page-toc
title: "ABM Campaign Tactic Processes"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Account Based Marketing Campaign Tactics
The Account Based Strategy team uses various tactics when launching an Account Based Marketing (ABM) campaign. This page will host the steps needed to take by an ABM Manager to successfully launch those tactics. 

### Buyer Progression Webcasts 
This is a GitLab-hosted virtual event with Webcast type configuration in Zoom. This is the preferred setup for larger GitLab-hosted virtual events (up to 1,000 attendees) that requires registration due to the integration with Marketo for automated lead flow and event tracking. GitLab-hosted webcast type is a single room virtual event that allows multiple hosts. Attendees cannot share audio/video unless manually grated access. 

#### Buyer Progression Webcast SLAs

Please note that all Buyer Progression Webcasts are considered webcasts with new content, which means they have a **-45 business day** SLA requirement. Please open this epic and confirm your speaker and date based on the epic task list prior to moving forward with other tasks. All other SLAs for the  Buyer Progression Webcast can be found [here] (https://docs.google.com/spreadsheets/d/1YXriQ1clvYyBn-TDbbCVvNP6NEbrAF-0w6tIHKhDeZM/edit#gid=1983708280)

#### Ownership of Buyer Progression Webcast Tasks

* Project Management in GitLab (epic, issue, timeline creation): ABM Manager
* GitLab-Hosted Webcast Calendar: ABM Manager
* Zoom setup and integration: ABM Manager
* Landing page and email copy (templated with limited customization, if any): ABM Manager
* Marketo program, SFDC campaign: ABM Manager
* Landing page, email creation: Verticurl
* Host dry run and run day-of event Zoom meeting: ABM Manager
* Day of event moderator: PMM/TMM/ABM Manager
     * The on-screen MC of the webcast (welcomes attendees, introduces presenters, reads questions from audience to the presenter to answer)

#### Buyer Progression Webcast Epic Code

```
> Naming convention: [Webcast Title] - [3-letter Month] [Webcast Date], [Year]

## Need to know
### Team
* [ ] ABM Manager: 
* [ ] Presenter:
* [ ] Q&A Coordinator:
* [ ] Q&A Support:
* [ ] Host:
### Date and Name
(don’t populate until officially confirmed)
* [ ] Dry run date and time: `MM-DD-YYYY and `
* [ ] Webcast date and time: `example - 10:00am PDT / 5:00pm UTC`
* [ ] Official webcast name: `webcast title`
* [ ] Topic: `topic`

## Content and resources
*  [Landing Page]  `to be added when live`
* Landing Page and Invite Copy 
* [Dry Run & Day of Agenda] 
* [Salesforce Campaign]
* [Marketo Program]()
* Campaign UTM - (Format: campaign tag - change to all lowercase, no spaces, hyphens, underscores, or special characters)

## Webcast date and speaker checklist 

NOTE: Please make sure your Webcast date and speaker are confirmed before starting the process for any of the other sections. Moving a Webcast date is extremely time-intensive once setup is complete.

* [ ] Check the [webcast gcal](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) and determine a date that is at least ~45 business days out. Best practice is to host a webcast on a Wednesday or Thursday. Ensure the suggested date does not over-saturate the calendar.
* [ ] Open PMM/TMM [speaker request issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=pmm-speaker-request) with your proposed date and topic.
* [ ] Once your speaker is confirmed, ensure the date is still available and add to [Webcast Google calendar] (https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) when final date is agreed upon - Remember to block the calendar for an additional 30 mins longer before and after the slotted time. Naming Convention for Calendar - [WC Hosted] Name of Webcast or [WS Hosted] Name of Workshop and Start-End Time/Time Zone of event (example - 9:00am - 12:00pm PST)
* [ ] Add speaker(s) and team to calendar invite and topic to the description.
* [ ] Uncheck the calendar settings `Modify Event` and `Invite Others` under Guest Permissions so invitees are not able to modify the event or add additional guests.
* [ ] Schedule dry run ~5 days before event and add to [Webcast Google calendar] (https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t)dry run with the same information as well as this epic linked. Only schedule for 60 minutes. Naming Convention for Calendar - [WC Dry Run] Name of Webcast Start-End Time/Time Zone of dry run (example - 9:00am - 10:00am PST)


### Webcast Preparation Checklist
ABM Manager to create, assign as needed and link below:

* [  ] [Webcast Prep Issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_prep) 
* [  ] [Program Tracking](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_program_tracking) 
* [  ] [Write Copy Issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_write_copy)
* [  ] [Target list creation Issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_target_list_creation)
* [  ] [Marketo Landing Page & Automation Issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_request_mkto_landing_page)
* [  ] [Sales Nominated Issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_request_sales_nominated)
* [  ] [Email Invitation Issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_request_email_invite)

Please Note: We do not send Marketo reminder emails for webcasts as Zoom sends a reminder the day before the webcast and an hour prior to the webcast.

* [  ] [Follow Up Email issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_request_email_follow_up)
* [  ] [Add to Nurture](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_request_add_nurture)
* [  ] [List Clean and Upload](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) - Zoom integration automatically uploads the registrants and their attendance status to SFDC but any notes, Q&A, etc. need to be added via this upload issue  - FMC creates, assigns to FMM and MOps
* [  ] [ABM Demandbase Campaign issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=ABM_Demandbase_Campaign)
* [  ] [ABM Campaign PathFactory Track](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=ABM_Campaign_PathFactory_Track)

/label ~"Webcast - GitLab Hosted" ~”Buyer Progression Webcast” ~"mktg-status::wip" ~"ABM Campaign"
```

#### Buyer Progression Webcast Setup
1. Step 1: [Configure Zoom](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#step-1-configure-zoom) - DRI: ABM Manager
1. Step 2: [Set up the webcast in Marketo/SFDC and integrate to Zoom](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#step-2-set-up-the-webcast-in-marketosfdc-and-integrate-to-zoom) - DRI: ABM Manager
1. Step 3A: [Update Marketo Tokens](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#step-3a-update-marketo-tokens) - DRI: ABM Manager and Verticurl
1. Step 3B: [Turn on Smart Campaigns in Marketo](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#step-3b-turn-on-smart-campaigns-in-marketo) - DRI: ABM Manager
1. Step 3C: [Create the Landing Page](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#step-3c-create-the-landing-page) - DRI: Verticurl
1. Step 4: [Webcast Invitations](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#step-4-webcast-invitations) - DRI:
Verticurl
1. Step 5: [Test Your Setup](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#step-5-test-your-setup) - DRI: ABM Manager

#### Post LIVE Webcast
1. [Converting the Webcast to an On-Demand Gated Asset](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#converting-the-webcast-to-an-on-demand-gated-asset) - DRI: ABM Manager
1. [Test your follow up emails and set to send](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#test-your-follow-up-emails-and-set-to-send) - DRI: Verticurl

