(function() {
  if (window.innerWidth > 992) {
    var staggerTime = 6;
    var duration = .8;
    var tl = new TimelineMax({ repeat: -1 });
    tl.staggerFrom('.testimonials-row', .6, {ease: Power4.easeOut, y: 40, opacity: 0, zIndex: 0}, staggerTime);
    tl.staggerTo('.testimonials-row', .6, {ease: Power4.easeOut, y: -40, opacity: 0, zIndex: 0}, staggerTime, staggerTime);
    tl.play();

    // var items = document.querySelectorAll('.testimonials-row'), length = items.length;
    // var duration = 1;
    // var tl = new TimelineMax({ paused:true, repeat: -1, delay: duration * 0.5 });
    // for (var i = 0; i < length; i += 1) {
    //   tl.from(items[i], duration, { opacity: 0, ease: Power4.easeOut, delay: duration * 4 });
    //   tl.to(items[i], duration, { opacity: 0, ease: Power4.easeOut, delay: duration * 4 });
    // }
    // tl.play();
  } else {
    // do nothing
  }
})();
